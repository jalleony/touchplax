package ru.crasiis.touchplax;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.HashMap;
import java.util.Map;

public class TouchplaxApp extends Application {
    String[] colors = {"red", "green", "blue", "yellow", "pink"};
    Map<Integer, Circle> touches = new HashMap<>();

    Stage stage;
    Scene scene;
    BorderPane mainPane;
    Label touchCounter;


    @Override
    public void start(Stage primaryStage) throws Exception{
        touchCounter = new Label("0");
        touchCounter.getStyleClass().add("counter");

        mainPane = new BorderPane();
        mainPane.getStyleClass().add("background");
        mainPane.setCenter(touchCounter);
        mainPane.setOnTouchPressed(event -> handlePaneTouchPressed(event));
        mainPane.setOnTouchMoved(event -> handlePaneTouchMoved(event));
        mainPane.setOnTouchReleased(event -> handlePaneTouchReleased(event));

        scene = new Scene(mainPane);
        scene.getStylesheets().add("ru/crasiis/touchplax/touchplax.css");

        stage = primaryStage;
        stage.setTitle("Touchplax v0.1");
        stage.setMinWidth(1024);
        stage.setMinHeight(768);
        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.show();
    }

    private void handlePaneTouchPressed(TouchEvent event) {
        Circle circle = new Circle(40);
        circle.setMouseTransparent(true);
        circle.getStyleClass().setAll("touchpoint-" +
                        colors[event.getTouchPoint().getId() % colors.length]
        );
        touches.put(event.getTouchPoint().getId(), circle);
        circle.setCenterX(event.getTouchPoint().getX());
        circle.setCenterY(event.getTouchPoint().getY());
        mainPane.getChildren().add(circle);
        touchCounter.setText(Integer.toString(event.getTouchCount()));
    }

    private void handlePaneTouchMoved(TouchEvent event) {
        Circle circle = touches.get(event.getTouchPoint().getId());
        circle.setCenterX(event.getTouchPoint().getX());
        circle.setCenterY(event.getTouchPoint().getY());
    }

    private void handlePaneTouchReleased(TouchEvent event) {
        Circle circle = touches.remove(event.getTouchPoint().getId());

        FadeTransition fadeOut = new FadeTransition(Duration.millis(500), circle);
        fadeOut.setFromValue(circle.getOpacity());
        fadeOut.setToValue(0.0);
        fadeOut.setOnFinished(e -> mainPane.getChildren().remove(circle));
        fadeOut.play();
        touchCounter.setText(Integer.toString(event.getTouchCount() - 1));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
